Student POC for c9
============================
create and start mongodb
----------------------------

>mkdir data

>echo 'mongod --bind_ip=$IP --dbpath=data --nojournal --rest "$@"' > mongod

>chmod a+x mongod

>./mongod


install dep and start the application
-----------------------------

>cd student-poc

>npm install

>npm start

CRUD operations example
---------------------------

>curl "https://student-poc-marales.c9users.io/student/1"

>curl -H "Content-Type: application/json" -X POST -d '{"name":"john snow","id":1,"marks":[5,6,8]}' https://student-poc-marales.c9users.io/student

>curl -H "Content-Type: application/json" -X PUT -d '{"name":"hodor hodor","marks":[10,6,9]}' https://student-poc-marales.c9users.io/student/1

>curl -X DELETE  https://student-poc-marales.c9users.io/student/1


references
-------------------------
+[express js](http://expressjs.com)
+[hjs npm](https://www.npmjs.com/package/hjs)
+[hjs homepage](http://twitter.github.io/hogan.js/)
+[cookie parser](https://github.com/expressjs/cookie-parser)
+[mongo setup](https://community.c9.io/t/setting-up-mongodb/1717)
+[mongoose](https://github.com/Automattic/mongoose)
+[mustache js](https://github.com/janl/mustache.js/)

