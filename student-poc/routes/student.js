var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var connection = mongoose.createConnection('mongodb://localhost/student');

var Student = connection.model('Student', { name: String, id : Number, marks : [] });

router.get('/:id', function(req, res, next) {
    console.log('GET | Student with id ' + req.params.id);
    Student.findOne({ 'id': req.params.id }, function (err, result) {
      if (err || result === null){
        res.status(404).send({ message : 'student not found' });
      }else{
        res.send({
          name : result.name,
          id : result.id,
          marks : result.marks
        });
      }
    });
});


router.post('/', function(req, res, next) {
  console.log('POST | Student : name ' + req.body.name + ' id ' + req.body.id + ' marks' + req.body.marks);
  var student = new Student({name : req.body.name , id : req.body.id, marks : req.body.marks});
  student.save(function (err) {
    if (err) {
      res.status(500).send({ message : 'student stored failed' });
    } else {
      res.send({ message : 'student stored' });
    }
  });

});


router.put('/:id', function(req, res, next) {
  console.log('PUT | Student with id ' + req.params.id);
  var student = req.body;
  Student.findOneAndUpdate({ id : req.params.id }, student, function(err, result){
    if(err || result === null){
      console.log(err)
      res.status(400).send({ message : 'student was not updated'});
    }else{
      res.send({ message : 'student was updated'});
    }
  });
});


router.delete('/:id', function(req, res, next) {
  console.log('DELETE | student with id ' + req.params.id);
  Student.findOneAndRemove({ id : req.params.id }, function(err, result){
    if(err || result === null){
      res.status(400).send({ message : 'student was not deleted'});
    }else{
      res.send({ message : 'student was deleted'});
    }
  });
});

module.exports = router;
