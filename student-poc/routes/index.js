var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var connection = mongoose.createConnection('mongodb://localhost/student');

var Student = connection.model('Student', { name: String, id : Number, marks : [] });

router.get('/', function(req, res, next) {
    Student.find({}, function(err, result){

        if(err){
            res.render('index', { title: 'Student POC', students : [] });
        }else{
                   console.log(result[0].name)
            res.render('index', { title: 'Student POC', students : result }); 
        }
    });
});

module.exports = router;
